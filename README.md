# Nonvisual Pong

Pong game for people with visual impairments made in C# Windows Console Application.

## Requirements (NuGet Packages)
- [SharpDX](https://www.nuget.org/packages/SharpDX)
- [SharpDX.XInput](https://www.nuget.org/packages/SharpDX.XInput)